<?php
/**
 * FILE:			Meta Capabilities ( map_meta_cap )
 * @package  		ChurchAmp_Ministries
 * @subpackage  	Includes
 * @version		5.0.0
 * @since			1.0.0
 * @author		Endeavr Media <support@endeavr.com>
 * @copyright		Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link			http://churchamp.com/plugins/ministries
 * @license		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

// mapping the meta capabilities
// @source: http://justintadlock.com/archives/2010/07/10/meta-capabilities-for-custom-post-types
add_filter( 'map_meta_cap', 'endvr_map_meta_cap_ministries', 10, 4 );
function endvr_map_meta_cap_ministries( $caps, $cap, $user_id, $args ) {
	/* If editing, deleting, or reading a ministry, get the post and post type object. */
	if ( 'edit_ministry' == $cap || 'delete_ministry' == $cap || 'read_ministry' == $cap ) {
		$post = get_post( $args[0] );
		$post_type = get_post_type_object( $post->post_type );

		/* Set an empty array for the caps. */
		$caps = array();
	}
	/* If editing a ministry, assign the required capability. */
	if ( 'edit_ministry' == $cap ) {
		if ( $user_id == $post->post_author )
			$caps[] = $post_type->cap->edit_posts;
		else
			$caps[] = $post_type->cap->edit_others_posts;
	}
	/* If deleting a ministry, assign the required capability. */
	elseif ( 'delete_ministry' == $cap ) {
		if ( $user_id == $post->post_author )
			$caps[] = $post_type->cap->delete_posts;
		else
			$caps[] = $post_type->cap->delete_others_posts;
	}
	/* If reading a private ministry, assign the required capability. */
	elseif ( 'read_ministry' == $cap ) {

		if ( 'private' != $post->post_status )
			$caps[] = 'read';
		elseif ( $user_id == $post->post_author )
			$caps[] = 'read';
		else
			$caps[] = $post_type->cap->read_private_posts;
	}
	/* Return the capabilities required by the user. */
	return $caps;
}