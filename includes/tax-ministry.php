<?php
/**
 * Taxonomy ( Register Ministry Scripture )
 *
 * @package  		ChurchAmp_Ministries
 * @subpackage  	Includes
 * @version  		5.0.0
 * @since   		1.0.0
 * @author  		Endeavr Media <support@endeavr.com>
 * @copyright  	Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link   		http://churchamp.com/plugins/ministries
 * @license  		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/* register and define the taxonomy on the 'init' hook */
/* @example: http://codex.wordpress.org/Function_Reference/register_taxonomy */
add_action( 'init', 'endvr_register_tax_ministriescripture' );
function endvr_register_tax_ministriescripture() {

	$labels = array(
		'name'                       	=> __( 'Ministry',                           		'churchamp-ministries' ),
		'singular_name'              	=> __( 'Scripture',                            		'churchamp-ministries' ),
		'menu_name'                  	=> __( 'Ministry',                           		'churchamp-ministries' ),
		'name_admin_bar'             	=> __( 'Ministry',                            		'churchamp-ministries' ),
		'search_items'               	=> __( 'Search '.'Ministry'.'',                    	'churchamp-ministries' ),
		'popular_items'              	=> __( 'Popular '.'Ministry'.'',                   	'churchamp-ministries' ),
		'all_items'                  	=> __( 'All '.'Ministry'.'',                       	'churchamp-ministries' ),
		'edit_item'                  	=> __( 'Edit '.'Scripture'.'',                       	'churchamp-ministries' ),
		'view_item'                  	=> __( 'View '.'Scripture'.'',                       	'churchamp-ministries' ),
		'update_item'                	=> __( 'Update '.'Scripture'.'',                     	'churchamp-ministries' ),
		'add_new_item'               	=> __( 'Add New '.'Scripture'.'',                    	'churchamp-ministries' ),
		'new_item_name'             	=> __( 'New '.'Scripture'.' Name',                	'churchamp-ministries' ),
		'separate_items_with_commas' 	=> __( 'Separate '.'Ministry'.' with Commas',      	'churchamp-ministries' ),
		'add_or_remove_items'        	=> __( 'Add or Remove '.'Ministry'.'',             	'churchamp-ministries' ),
		'choose_from_most_used'      	=> __( 'Choose from the Most Used '.'Ministry'.'',	'churchamp-ministries' ),
	);
	/* only 2 caps are needed: 'manage_ministries' and 'edit_ministries'. */
	$capabilities = array(
		'manage_terms' 			=> 'manage_ministries',
		'edit_terms'   			=> 'manage_ministries',
		'delete_terms' 			=> 'manage_ministries',
		'assign_terms' 			=> 'edit_ministries',
	);
	$rewrite = array(
		'slug'         			=> 'ministries/ministry',
		'with_front'   			=> false,
		'hierarchical' 			=> false,
		'ep_mask'      			=> EP_NONE,
	);
	$args = array(
		'public'            		=> true,
		'show_ui'           		=> true,
		'show_in_nav_menus' 		=> true,
		'show_tagcloud'     		=> false,
		'show_admin_column' 		=> true,
		'hierarchical'      		=> true,
		'query_var'         		=> 'ministry',
		'capabilities' 			=> $capabilities,
		'rewrite' 				=> $rewrite,
		'labels' 					=> $labels,
	);

	/* register the 'ministriescripture' taxonomy. */
	register_taxonomy( 'ministry', array( 'ministries' ), $args );
}