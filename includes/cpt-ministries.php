<?php
/**
 * Custom Post Type ( Register the Ministries CPT )
 *
 * @package  		ChurchAmp_Ministries
 * @subpackage  	Includes
 * @version  		5.0.0
 * @since   		1.0.0
 * @author  		Endeavr Media <support@endeavr.com>
 * @copyright  	Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link   		http://churchamp.com/plugins/ministries
 * @license  		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/* register and define the custom content type on the 'init' hook */
/* @ example: http://codex.wordpress.org/Function_Reference/register_post_type */
add_action( 'init', 'endvr_register_cpt_ministries' );
function endvr_register_cpt_ministries()
{
	/* labels used when displaying the posts in the admin */
	$labels = array(
		'name'               	=> __( 'Ministries',                   	'churchamp-ministries' ),
		'singular_name'      	=> __( 'Ministry',                    	'churchamp-ministries' ),
		'menu_name'          	=> __( 'Ministries',                      	'churchamp-ministries' ),
		'name_admin_bar'     	=> __( 'Ministries',                    	'churchamp-ministries' ),
		'add_new'            	=> __( 'Add New',                      	'churchamp-ministries' ),
		'add_new_item'       	=> __( 'Add New Ministry',            	'churchamp-ministries' ),
		'edit_item'          	=> __( 'Edit Ministry',               	'churchamp-ministries' ),
		'new_item'           	=> __( 'New Ministry',                	'churchamp-ministries' ),
		'view_item'          	=> __( 'View Ministry',               	'churchamp-ministries' ),
		'search_items'       	=> __( 'Search Ministries',               	'churchamp-ministries' ),
		'not_found'          	=> __( 'No Ministries Found',          	'churchamp-ministries' ),
		'not_found_in_trash' 	=> __( 'No Ministries Found in Trash', 	'churchamp-ministries' ),
		'all_items'          	=> __( 'Ministries',                   	'churchamp-ministries' ),
		'parent_item_colon'  	=> __( 'Parent Directory', 			'churchamp-ministries' ),
		// custom labels because WordPress doesn't have anything to handle this
		'archive_title'		=> __( 'Ministries',					'churchamp-ministries' ),
	);
	$capability_type = array(
		'ministry',
		'ministries',
	);
	/* only 3 caps are needed: 'manage_ministries', 'create_ministries', 'edit_ministries' */
	$capabilities = array(
		// meta caps (don't assign these to roles)
		'edit_post'     		=> 'edit_ministry',
		'read_post'    		=> 'read_ministry',
		'delete_post'   		=> 'delete_ministry',
		// primitive/meta caps
		'create_posts'			=> 'create_ministries',
		// primitive caps used outside of map_meta_cap()
		'edit_posts'   		=> 'edit_ministries',
		'edit_others_posts'  	=> 'manage_ministries',
		'publish_posts'  		=> 'manage_ministries',
		'read_private_posts'  	=> 'read',
		// primitive caps used inside of map_meta_cap()
		'read'				=> 'read',
		'delete_posts'			=> 'manage_ministries',
		'delete_private_posts' 	=> 'manage_ministries',
		'delete_published_posts' => 'manage_ministries',
		'delete_others_posts'	=> 'manage_ministries',
		'edit_private_posts' 	=> 'edit_ministries',
		'edit_published_posts' 	=> 'edit_ministries',
	);
	$supports = array(
		'title',
		'editor',
		'page-attributes',
		'thumbnail',
	);
	$taxonomies = array(
		'ministry',
	);
	$rewrite = array(
		'slug'				=> 'ministries',
		'with_front'			=> false,
		'pages'				=> true,
		'feeds'				=> true,
		'ep_mask'				=> EP_PERMALINK,
	);
	$args = array(
		'description'			=> '',
		'public'    			=> true, 		// several of the other variables derive their values from 'public' by default
		'exclude_from_search'	=> false,
		'publicly_queryable' 	=> true,
		'can_export'  			=> true,
		'show_ui'    			=> true,
		'show_in_admin_bar' 	=> true,
		'show_in_nav_menus' 	=> true,
		'show_in_menu'   		=> true,
		'menu_position'  		=> 35, 		// should be an integer between 26 and 58 to appear between Comments and Appearance in admin menu
		'menu_icon'   			=> '', 		// keep this line because it adds the .menu-icon-ministries class to the <li> HTML
		'hierarchical'   		=> true,		// make this true only if you want the content type to function like a page instead of a post
		'delete_with_user'		=> true,		// @source: https://github.com/justintadlock/custom-content-portfolio/
		'has_archive'   		=> 'ministries', 	// must be explicitly set in order for rewrite rules to work
		'query_var'   			=> 'ministries',
		'map_meta_cap'  		=> true, 		// this should be set to false if you wish to set up custom mapping of meta capabilities
		'capability_type'  		=> $capability_type,
		'capabilities'  		=> $capabilities,
		'labels'    			=> $labels,
		'supports'   			=> $supports,
		'taxonomies'   		=> $taxonomies,
		'rewrite'    			=> $rewrite,
	);
	register_post_type( 'ministries', $args );
}